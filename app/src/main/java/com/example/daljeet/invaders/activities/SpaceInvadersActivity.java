package com.example.daljeet.invaders.activities;

import android.app.Activity;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.view.Display;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.daljeet.invaders.R;

import java.util.Timer;
import java.util.TimerTask;


        //-------------------- This is main activity-------------------//
public class SpaceInvadersActivity extends Activity implements MyListener {

    private SpaceInvadersView spaceInvadersView;
    private MediaPlayer ring;
    private Display display;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ring = MediaPlayer.create(SpaceInvadersActivity.this, R.raw.background);
        ring.start();

        display = getWindowManager().getDefaultDisplay();
        createHome();
    }

    @Override
    public void recreate() {
        super.recreate();

        ring = MediaPlayer.create(SpaceInvadersActivity.this, R.raw.background);
        ring.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        spaceInvadersView.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        spaceInvadersView.pause();
    }

    public void createHome() {

        Point size = new Point();
        display.getSize(size);

        spaceInvadersView = new SpaceInvadersView(getApplicationContext(), size.x, size.y, this);
        setContentView(R.layout.menu);


        final Button playbutton = (Button) findViewById(R.id.play_button);
        playbutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (ring != null) {
                    ring.stop();
                }
                setContentView(spaceInvadersView);
            }
        });


    }




    @Override

    protected void onDestroy() {
        super.onDestroy();
        if (ring != null) {
            ring.stop();
        }

        MyTimerTask myTask = new MyTimerTask();
        Timer myTimer = new Timer();

        myTimer.schedule(myTask, 360, 8640);
    }


    @Override
    public void onStartLevel(String message) {
        runOnUiThread(new ShowToastRunnable(this, message));
    }

    @Override
    public void popup() {
        runOnUiThread(new ShowPopupRunnable(this, spaceInvadersView));
    }



    class MyTimerTask extends TimerTask {
        public void run() {

        }
    }



    @Override
    public void onEndGame(int score) {

    }




    class GetPlayerNameRunnable implements Runnable {
        private Activity activity;
        private int score;

        public GetPlayerNameRunnable(Activity activity, int score) {
            this.activity = activity;
            this.score = score;
        }

        @Override
        public void run() {


        }
    }

    class ShowToastRunnable implements Runnable {
        private Activity activity;
        private String message;

        public ShowToastRunnable(Activity activity, String message) {
            this.activity = activity;
            this.message = message;
        }

        @Override
        public void run() {
            Toast.makeText(this.activity, message,
                    Toast.LENGTH_LONG).show();
        }
    }

    class ShowPopupRunnable implements Runnable {
        View view;
        Activity activity;

        public ShowPopupRunnable(Activity activity, View view) {
            this.view = view;
            this.activity = activity;
        }

        @Override
        public void run() {

            PopupMenu popup = new PopupMenu(activity, view, Gravity.RIGHT, R.attr.actionOverflowMenuStyle, 0);
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.resume:
                            spaceInvadersView.resume();
                            return true;
                        case R.id.restart:
                            spaceInvadersView.restart();
                            return true;
                        case R.id.quit:
                            recreate();
                            return true;
                    }

                    return true;
                }
            });
            popup.getMenuInflater().inflate(R.menu.my_menu, popup.getMenu());
            popup.show();
        }
    }


}

