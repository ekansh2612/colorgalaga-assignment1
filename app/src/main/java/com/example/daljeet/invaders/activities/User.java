package com.example.daljeet.invaders.activities;


public class User {

    private int userId;
    private String userName;
    private int userScore;


    public User(int userId, String userName, int userScore) {
        this.userId = userId;
        this.userName = userName;
        this.userScore = userScore;
    }


    @Override
    public String toString() {
        return userName + " Scored " + userScore;
    }
}

