package com.example.daljeet.invaders.activities;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.media.MediaPlayer;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.example.daljeet.invaders.R;

import java.util.concurrent.CopyOnWriteArrayList;


        //-----------------------------This is game engine--------------//
public class SpaceInvadersView extends SurfaceView implements Runnable {

    MyListener myListener;

    Context context;

    CopyOnWriteArrayList<Invader> invaders = new CopyOnWriteArrayList<>();

    int score = 0;
    private Thread gameThread = null;
    private SurfaceHolder ourHolder;

    private volatile boolean playing;
   
    private boolean paused = true;
    private boolean endGame = false;
    private Canvas canvas;
    private Paint paint;

    private long fps;

    private long timeThisFrame;

    private int screenX;
    private int screenY;

    private PlayerShip playerShip;

    private CopyOnWriteArrayList<Bullet> bullets = new CopyOnWriteArrayList<>();
    private long lastBulletFiredTime;
    private CopyOnWriteArrayList<Bullet> invadersBullets = new CopyOnWriteArrayList<>();

    private int lives = 3;
    private int level = 1;

    final MediaPlayer death = MediaPlayer.create(getContext(), R.raw.explosion);
    final MediaPlayer shoot = MediaPlayer.create(getContext(), R.raw.shoot);


    public SpaceInvadersView(Context context, int x, int y, MyListener myListener) {

        super(context);

        playing = true;
        endGame = false;

        this.myListener = myListener;
        this.context = context;


        ourHolder = getHolder();
        paint = new Paint();

        screenX = x;
        screenY = y;

        lives = 3;
        score = 0;
        level = 1;

        prepareLevel();
    }


    private void prepareLevel() {
        if (level > 1) {
            myListener.onStartLevel("Starting level " + level);
        }

        playerShip = new PlayerShip(context, screenX, screenY);
        playerShip.increaseSpeed(level);


        for (Bullet invaderBullet : invadersBullets) {
            invaderBullet = new Bullet(screenY);
        }

        invaders = new CopyOnWriteArrayList<>();

        for (int column = 0; column < 6; column++) {
            for (int row = 0; row < (level + 1); row++) {
                invaders.add(new Invader(context, row, column, screenX, screenY, level));
            }
        }
        bullets = new CopyOnWriteArrayList<>();
        invadersBullets = new CopyOnWriteArrayList<>();
    }

    @Override
    public void run() {
        while (playing) {


            long startFrameTime = System.currentTimeMillis();
            if (!paused && !endGame) {
                update();
            }


            draw();

            timeThisFrame = System.currentTimeMillis() - startFrameTime;
            if (timeThisFrame >= 1) {
                fps = 1000 / timeThisFrame;
            }
        }
    }

    private void update() {
        boolean bumped = false;



        playerShip.update(fps);


        for (Invader invader : invaders) {
            if (invader != null && invader.getVisibility()) {

                invader.update(fps);


                if (invader.takeAim(playerShip.getX(), playerShip.getLength())) {

                    Bullet invaderBullet = new Bullet(screenY);
                    if (invaderBullet.shoot(invader.getX() + invader.getLength() / 2, invader.getY(), Bullet.DOWN)) {
                        invadersBullets.add(invaderBullet);
                    }
                }
                // If that move caused them to bump the screen change bumped to true
                if (invader.getX() > screenX - invader.getLength()
                        || invader.getX() < 0) {

                    bumped = true;
                }
            }

        }


        for (Bullet invaderBullet : invadersBullets) {
            if (invaderBullet.getStatus()) {
                invaderBullet.update(fps);
            }
        }


        if (bumped) {


            for (Invader invader : invaders) {
                invader.dropDownAndReverse();

                if (invader.getY() > screenY - screenY / 10) {
                    death.start();
                    if (score > 300) {
                        myListener.onEndGame(score);
                        playing = false;
                    }

                    endGame = true;
                    lives = 3;
                    score = 0;
                    level = 1;
                    prepareLevel();
                }
            }
        }

        int enemyCount = -1;

        for (Bullet bullet : bullets) {
            enemyCount = 0;
            if (bullet.getStatus()) {
                bullet.update(fps);
            }

            if (bullet.getImpactPointY() < 0) {
                bullet.setInactive();
                bullets.remove(bullet);
                continue;
            }

            if (bullet.getStatus()) {
                for (Invader invader : invaders) {
                    if (invader.getVisibility()) {
                        if (RectF.intersects(bullet.getRect(), invader.getRect())) {
                            invader.explode();
                            bullet.setInactive();
                            bullets.remove(bullet);
                            score = score + 10;
                            break;
                        } else {
                            enemyCount++;
                        }
                    }
                }
            }
        }
        // Player wins when there are no more enemyes and endgame happens
        if (enemyCount == 0) {
            level++;
            prepareLevel();
        }

        // When bullets get out of screen they should disappear.
        for (Bullet invaderBullet : invadersBullets) {
            if (invaderBullet.getImpactPointY() > screenY) {
                invaderBullet.setInactive();
                invadersBullets.remove(invaderBullet);
            }
        }

        // Bullet collision, when a bullet hits the rect of playership player loses one life.
        for (Bullet invaderBullet : invadersBullets) {
            if (invaderBullet.getStatus()) {
                if (RectF.intersects(playerShip.getRect(), invaderBullet.getRect())) {
                    invaderBullet.setInactive();
                    invadersBullets.remove(invaderBullet);
                    lives--;


                    if (lives == 0) {
                        if (score > 300) {
                            myListener.onEndGame(score);
                            playing = false;
                        }
                        death.start();

                        endGame = true;
                        lives = 3;
                        score = 0;
                        level = 1;
                        prepareLevel();
                    }
                }
            }
        }
    }

    private void draw() {
        if (ourHolder.getSurface().isValid()) {

            canvas = ourHolder.lockCanvas();


            canvas.drawColor(Color.argb(255, 26, 128, 182));
            paint.setColor(Color.argb(255, 255, 255, 255));


            canvas.drawBitmap(playerShip.getBitmap(), playerShip.getX(), playerShip.getY(), paint);


            for (Invader invader : invaders) {
                if (invader.getVisibility()) {
                    if (invader.isExploded()) {
                        if (System.currentTimeMillis() - invader.getExplodedTime() < 200) {
                            canvas.drawBitmap(invader.getBitmap3(), invader.getX(), invader.getY(), paint);
                        } else {
                            invaders.remove(invader);
                        }
                    } else {
                        canvas.drawBitmap(invader.getBitmap2(), invader.getX(), invader.getY(), paint);
                    }
                }
            }


            for (Bullet bullet : bullets) {
                if (bullet.getStatus()) {
                    paint.setColor(Color.YELLOW);
                    canvas.drawRect(bullet.getRect(), paint);
                }
            }


            paint.setColor(Color.RED);
            for (Bullet invaderBullet : invadersBullets) {
                canvas.drawRect(invaderBullet.getRect(), paint);
            }


            paint.setColor(Color.argb(255, 249, 129, 0));
            paint.setTextSize(80);
            canvas.drawText("Score: " + score + "   Lives: " + lives, 20, 80, paint);

            if (endGame) {

                canvas.drawBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.gameover), 200, screenY / 2 - 400, paint);
            }

            canvas.drawBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.pause), screenX - 120, 30, paint);


            ourHolder.unlockCanvasAndPost(canvas);
        }
    }


    public void pause() {
        playing = false;

        }



    public void resume() {
        playing = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    public void restart() {
        lives = 3;
        score = 0;
        level = 1;
        prepareLevel();
        resume();
    }



    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {

            case MotionEvent.ACTION_DOWN:
                paused = false;
                if (endGame) {
                    if ((motionEvent.getX() > 200) && (motionEvent.getX() < screenX * 2 - 200) && (motionEvent.getY() > screenY / 3) && (motionEvent.getY() < screenY * 2 / 3)) {
                        endGame = false;

                        new SpaceInvadersView(context, screenX, screenY, myListener);
                    }
                } else {
                    if ((motionEvent.getX() > screenX - 130) && (motionEvent.getX() < screenX - 30) && (motionEvent.getY() > 30 && (motionEvent.getY() < 130))) {
                        pause();
                        myListener.popup();

                    }
                    if (motionEvent.getY() > screenY * 0.5) {
                        if (motionEvent.getX() > screenX / 2) {
                            playerShip.setMovementState(playerShip.RIGHT);
                        } else {
                            playerShip.setMovementState(playerShip.LEFT);
                        }
                    }
                    Bullet bullet = new Bullet((int) playerShip.getY());
                    if (System.currentTimeMillis() - lastBulletFiredTime > 100 && bullet.shoot(playerShip.getX() +
                            playerShip.getLength() / 2, screenY, Bullet.UP)) {
                        shoot.start();
                        bullets.add(bullet);
                        lastBulletFiredTime = System.currentTimeMillis();
                    }
                }
                break;


            case MotionEvent.ACTION_UP:
                if (motionEvent.getY() > screenY * 0.8) {
                    playerShip.setMovementState(playerShip.STOPPED);
                }
                break;
        }
        return true;
    }


}
