package com.example.daljeet.invaders.activities;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.RectF;

import com.example.daljeet.invaders.R;


public class PlayerShip {

    RectF rect;

    private Bitmap bitmap;


    private float length;
    private float height;


    private float x;


    private float y;
    private float shipSpeed;
    private final float baseSpeed = 205;


    public final int STOPPED = 0;
    public final int LEFT = 1;
    public final int RIGHT = 2;


    private int shipMoving = STOPPED;

    private float width;

    public PlayerShip(Context context, int screenX, int screenY){

        rect = new RectF();

        length = screenX / 10;
        height = screenY / 10;


        x = screenX / 2 - 125;
        y = screenY / 9 * 8;
        width = screenX;


        bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.ship);

        bitmap = Bitmap.createScaledBitmap(bitmap,
                (int) (length),
                (int) (height),
                false);


        shipSpeed = baseSpeed;
    }
    public RectF getRect(){
        return rect;
    }

    public Bitmap getBitmap(){
        return bitmap;
    }

    public float getX(){
        return x;
    }

    public float getY(){
        return y;
    }

    public float getLength(){
        return length;
    }


    public void setMovementState(int state){
        shipMoving = state;
    }


    public void update(long fps){
        if(shipMoving == LEFT && x - shipSpeed / fps > 0){
            x = x - shipSpeed / fps;
        }

        if(shipMoving == RIGHT && x + length + shipSpeed / fps < width){
            x = x + shipSpeed / fps;
        }


        rect.top = y;
        rect.bottom = y + height;
        rect.left = x;
        rect.right = x + length;

    }

    public void increaseSpeed(int level) {
        this.shipSpeed = baseSpeed + (level - 1) * 10;
    }
}

