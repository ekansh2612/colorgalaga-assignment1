package com.example.daljeet.invaders.activities;



public interface MyListener {
        void onEndGame(int score);

        void onStartLevel(String message);

        void popup();
}
